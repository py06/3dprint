largeur_bac=175;

module grip() {
    cube([3, 12, 14], center=true);
    translate([-2, 0, -3.5]) cube([2, 12, 2], center=true);
    
}

cube([largeur_bac, 12, 3], center=true);
translate([largeur_bac/2-1.4, 0, -5.5]) grip();
translate([-largeur_bac/2+1.4, 0, -5.5]) mirror([1, 0, 0]) grip();
