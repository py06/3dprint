epaisseur_barreau = 7; //epaisseur du barreau de la jardiniere (em mm)
longeur_cale = 115; //longeur entre le mur et le barreau de la jardiniere (en mm)
hauteur_base = 60;
angle_barreau = 8;

module ventouse()
{
    difference() {
        translate([0, -5.5, -5]) cube([14,11,10]);
        rotate([0, 90, 0]) {
            cylinder($fn=20, h=6, d=6.3);
            translate([0, 0, 3]) cylinder($fn=20, h=3, d=9);
            translate([0, 0, -2]) cylinder($fn=20, h=3, d=9);
        }
    }
    translate([5.7,-5.5,-5]) cube([4,11,10]);
}

module ailette(hauteur) {
    rotate([0, 0, 20]) cube([1, hauteur/3+1, hauteur]);
}

module ailettes(largeur, hauteur, epaisseur) {
    translate([epaisseur/2 + 1.9, 0, 0]) ailette(hauteur);
    translate([epaisseur/2 + 1.9, 0, 0]) mirror([0, 1, 0]) ailette(hauteur);
}

function epaisseur(e, marge) = e + 2*marge;

module socle_vertical(largeur, hauteur, epaisseur) {
    translate([0, -epaisseur/2, 15]) cube([largeur, epaisseur, hauteur-15]);
    translate([-0, 0, 10])  ventouse();
    translate([0, -epaisseur/2, 0])cube([largeur, epaisseur, 5]);
    
    ailettes(largeur, hauteur, epaisseur);
    translate([0, -20, 0]) cube([2, 15, hauteur] );
    translate([0, 5, 0]) cube([1, 15, hauteur] );

}

module renfort_potence(longeur, epaisseur) {
    translate([longeur/2+epaisseur/2, 0, 20]) rotate([0, 26, 0]) cube([longeur, epaisseur, epaisseur], center=true);
}

module cale(long, epaisseur) {
    epaisseur = epaisseur(epaisseur_barreau, 2);
    largeur = epaisseur + 3;
    socle_vertical(largeur, hauteur_base, epaisseur);
    difference() {
        translate([13, -epaisseur/2, 0]) cube([long-12, epaisseur, largeur]);
	
        $fn=20;
        translate([long-1.7, 0, -2]) {
            rotate([0,angle_barreau,0]) color("green") cylinder(h=largeur+3, d1=epaisseur_barreau+1,     d2=epaisseur_barreau+1);
        }
    }
    renfort_potence(60, epaisseur);
}

cale(longeur_cale, epaisseur_barreau);
