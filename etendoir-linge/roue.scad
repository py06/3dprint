module cale() {
    translate([9,18,37]) {
        cylinder(12, 7, 6.5);
    }
    translate([9,18,30]) {
        cylinder(10, 7, 6.5);
    }
    intersection(){
        cube([35, 35, 35]);
        translate([17.5,17.5,17.5]) {
            sphere(r=22);
        }
    }    
}

cale();
